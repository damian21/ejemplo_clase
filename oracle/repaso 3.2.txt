select last_name, salary, hire_date from employees where 
hire_date=to_date('06-1987-17','mm-RRRR-dd')

SELECT last_name, UPPER(CONCAT(SUBSTR (LAST_NAME, 1, 8), '_US'))
 FROM   employees WHERE  department_id = 60;

select last_name,salary,job_id,nvl(to_char(commission_pct),'no tiene') from employees
where department_id=50

select last_name, salary, job_id,nvl2(commission_pct,'Tiene','No tiene') from employees

select last_name, salary, job_id, coalesce(commission_pct,manager_id,2) from employees

select last_name,salary,job_id, nullif(employee_id,employee_id*(nvl(commission_pct,1)))"Nullif" from employees

select last_name, job_id,department_id,
case department_id when 30 then 'financiera'
                   when 50 then 'administracion'
                   when 90 then 'directivo maricon'
                   else 'fracasado' end  "case" from employees

select last_name, lpad(salary, 15, '$') as Salary from employees

select last_name,nvl(to_char(commission_pct),'No commission') from employees

select job_id,
case job_id        when 'AD_PRES' then 'A'
                   when 'ST_MAN' then 'B'
                   when 'IT_PROG'  then 'C'
                   when 'SA_REP' then 'D'
                   when 'ST_CLERK' then 'E'
                   else '0' end  "GRA" from employees