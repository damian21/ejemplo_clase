ejercicio 1
select last_name, salary from employees where salary > 12000

ejercicio 2
select last_name,employee_id from employees where employee_id = 176

ejercicio 3
select last_name,salary  from employees where salary not between  5000 and 12000

ejercicio 4
select last_name,job_id,hire_date from employees where last_name in ('Matos','Taylor') order by hire_date desc

ejercicio 5
select last_name,department_id from employees where department_id in (20,50) order by last_name asc

ejercicio 6
select last_name "apellido", salary "salario mensual" from employees where salary between 5000 and 12000 and (department_id = 20 or department_id = 50)

ejercicio 7
select last_name,hire_date from employees where hire_date between '01/01/94' and '31/12/94'

ejercicio 8
select last_name,job_id from employees where manager_id is null

ejercicio 9
select last_name,salary,commission_pct from employees where commission_pct is not null order by salary desc,commission_pct desc

ejercicio 10
select last_name,salary from employees where salary > :salario

ejercicio 11
select employee_id,last_name,salary,department_id from  employees where manager_id = :supervisor order by :camporden

ejercicio 12
select last_name from employees where last_name like '__a%'

ejercicio 13
select last_name from employees where last_name like '%a%' and last_name like'%e%'

ejercicio 14
select last_name ,job_id,salary from employees where job_id = 'SA_REP' or  job_id = 'ST_CLERK' and salary not in (2500,3500,7000)

ejercicio 15
select last_name,salary,commission_pct from employees where commission_pct like '%,2'



